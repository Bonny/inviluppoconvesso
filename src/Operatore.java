/*
classe contenente dei metodi su supporto all'app
 */

import java.util.ArrayList;

public class Operatore {

    private final int MAX = 500;
    private int i;

    /*dati due puni e una retta verifica se i punti
    stanno dalla stessa parte rispetto alla retta*/
    public boolean stessaParte(Retta R, Punto P, Punto Q) {

        double dx, dy, dx1, dx2, dy1, dy2;
        dx = R.getP2().getX() - R.getP1().getX();
        dy = R.getP2().getY() - R.getP1().getY();
        dx1 = P.getX() - R.getP1().getX();
        dy1 = P.getY() - R.getP1().getY();
        dx2 = Q.getX() - R.getP2().getX();
        dy2 = Q.getY() - R.getP2().getY();
        return ((((dx * dy1) - (dy * dx1)) * ((dx * dy2) - (dy * dx2))) >= 0);
    }
    /* crea e ritorna un insieme di punti (x,y) tutti diversi*/

    public ArrayList<Punto> creaInsieme(int dim) {

        Punto v[] = new Punto[dim];
        for (i = 0; i < v.length; i++) {
            v[i] = new Punto();
        }
        insieme(v, dim - 1, dim);
        ArrayList<Punto> list = new ArrayList<Punto>();
        for (i = 0; i < v.length; i++) {
            list.add(v[i]);
        }

        return list;
    }

    private void insieme(Punto v[], int off, final int dim) {

        if (off >= 0) {

            boolean flag = false;
            final int x = (int) (Math.random() * MAX) + 35;
            final int y = (int) (Math.random() * MAX) + 15;
            for (int k = off + 1; k < dim; k++) {
                if (v[k].x == x && v[k].y == y) {
                    flag = true;
                }
            }
            if (flag == true) {
                insieme(v, off, dim);
            } else {
                v[off].x = x;
                v[off].y = y;
                insieme(v, off - 1, dim);
            }
        }
    }

    /*
    p1 = ordinata minima e ascissa massima
    facendo ruotare in seenso anti orario (0 a 2PI) la retta
    passante per p1 ordino i punti del piano
    rimanenti in base all'angolo di rotazione
     */
    public Punto getP1(ArrayList<Punto> Q) {

        int k = 0;
        Punto tmp = Q.get(k);
        for (i = 1; i < Q.size(); i++) {
            if (Q.get(i).y < tmp.y) {
                tmp = Q.get(i);
                k = i;
            } else if (Q.get(i).y == tmp.y) {
                if (tmp.x < Q.get(i).x) {
                    tmp = Q.get(i);
                    k = i;
                }
            }
        }
        Q.remove(k);
        return tmp;
    }
}
