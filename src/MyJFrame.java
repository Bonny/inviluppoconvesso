
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.ArrayList;
import javax.swing.*;

public class MyJFrame extends JFrame implements Runnable {

    private boolean stato = false;
    private Thread myThread;
    private Operatore op;
    private ArrayList<Punto> insieme;
    private JPanel canvas, menu;
    private JButton crea, avvia;
    private JComboBox combo;
    private int i;
    private final int NUM_POINT = 30, F_WIDTH = 600, F_HEIGHT = 600, TIMESTAMP = 1300;
    private Graphics2D g;

    public MyJFrame() {

        super("Inviluppo Covesso");
        myThread = new Thread(this);
        op = new Operatore();
        canvas = new JPanel();
        menu = new JPanel();
        crea = new JButton("Genera");
        avvia = new JButton("Start");
        combo = new JComboBox();
        insieme = new ArrayList<Punto>();
        for (i = 6; i <= NUM_POINT; i += 3) {
            combo.addItem((Object) i);
        }
        canvas.setBackground(Color.white);
        menu.setLayout(new FlowLayout());
        menu.add(combo);
        menu.add(crea);
        menu.add(avvia);
        this.getContentPane().add("Center", canvas);
        this.getContentPane().add("South", menu);

        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Toolkit mioToolkit = Toolkit.getDefaultToolkit();
        Dimension dimSchermo = mioToolkit.getScreenSize();
        setBounds((int) (dimSchermo.getWidth() / 2) - F_WIDTH / 2, (int) (dimSchermo.getHeight() / 2) - F_HEIGHT / 2, F_WIDTH, F_HEIGHT);

        Click_Button_crea();
        Click_Button_avvia();

    }

    private void Click_Button_crea() {
        crea.addActionListener(
                new ActionListener() {

                    /*crea l'insieme di punti e li ordina*/
                    public void actionPerformed(ActionEvent event) {
                        if (!stato) {
                            int dim = Integer.parseInt(combo.getSelectedItem().toString());
                            insieme = op.creaInsieme(dim);
                            g = (Graphics2D) canvas.getGraphics();
                            canvas.update(g);
                            disegnaAssi(g);
                            disegnaPunti(insieme, g);
                            Punto p1 = op.getP1(insieme);
                            Retta R[] = new Retta[insieme.size()];
                            for (i = 0; i < insieme.size(); i++) {
                                R[i] = new Retta(p1, insieme.get(i));
                            }
                            insieme.clear();
                            Retta.sort(R);
                            insieme.add(p1);
                            for (i = 0; i < R.length; i++) {
                                insieme.add(R[i].getP2());
                            }
                        }
                    }
                });
    }

    private void Click_Button_avvia() {
        avvia.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent event) {
                        if (!stato && !insieme.isEmpty()) {
                            stato = true;
                            new Thread(myThread).start();
                        }
                    }
                });
    }

    private void disegnaPunti(ArrayList<Punto> Q, Graphics2D g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, 12));
        Punto ts;
        for (i = 0; i < Q.size(); i++) {
            ts = trasforma(Q.get(i));
            g.fill(ts.getArea());
        }
    }

    /*evidenzia il punto P1 ovvero il primo vertice dell'inviluppo
    e traccia la retta passante per P1 e parallela all'asse x*/
    private void disegnaP1(Punto p, Graphics2D g) {
        Punto ts = trasforma(p);
        g.setFont(new Font("Arial", Font.BOLD, 12));
        g.setColor(Color.BLACK);
        g.drawString("p", ts.x, ts.y + 22);
        g.setColor(Color.RED);
        float dash[] = {10.0f};
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
        g.drawLine(0, ts.y, canvas.getWidth(), ts.y);
        g.setStroke(new BasicStroke());
        g.setColor(Color.ORANGE);
        g.fill(ts.getArea());
        g.setColor(Color.BLACK);
        g.draw(ts.getBordo());
    }

    /*disegna il piano cartesiano*/
    private void disegnaAssi(Graphics2D g) {
        int w = canvas.getWidth(), h = canvas.getHeight();
        g.setFont(new Font("Arial", Font.BOLD, 13));
        g.setColor(Color.BLACK);
        g.drawLine(12, 0, 12, h);
        g.drawLine(0, h - 12, w, h - 12);
        GeneralPath trx = new GeneralPath();
        trx.moveTo(12, 0);
        trx.lineTo(18, 15);
        trx.lineTo(5, 15);
        trx.closePath();
        g.fill(trx);
        g.drawString("y", 20, 10);
        GeneralPath tr = new GeneralPath();
        tr.moveTo(w, h - 12);
        tr.lineTo(w - 15, h - 5);
        tr.lineTo(w - 15, h - 18);
        tr.closePath();
        g.fill(tr);
        g.drawString("x", w - 10, h - 20);
    }

    public void run() {
        int k;
        Punto t = null;
        Punto v = trasforma(insieme.get(0));
        /* simulazione della retta passante per P1 ruota in senso anti orario*/
        for (i = 1; i < insieme.size(); i++) {
            for (k = 1; k < i; k++) {
                t = trasforma(insieme.get(k));
                g.setColor(Color.WHITE);
                g.drawLine(v.x, v.y, t.x, t.y);
                g.setColor(Color.BLACK);
                g.fill(t.getArea());
            }
            g.setColor(Color.BLACK);
            t = trasforma(insieme.get(i));
            g.drawString(String.valueOf(i), t.x + 10, t.y + 10);
            g.setColor(Color.MAGENTA);
            g.drawLine(v.x, v.y, t.x, t.y);
            disegnaP1(insieme.get(0), g);
            try {
                Thread.sleep(TIMESTAMP);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }

        t = trasforma(insieme.get(insieme.size() - 1));
        g.setColor(Color.WHITE);
        g.drawLine(v.x, v.y, t.x, t.y);
        g.drawLine(0, v.y, canvas.getWidth(), v.y);
        g.setColor(Color.BLACK);
        for (i = 1; i < insieme.size(); i++) {
            t = trasforma(insieme.get(i));
            g.fill(t.getArea());
            g.drawString(String.valueOf(i), t.x + 10, t.y + 10);
        }
        /*fine simulazione*/
        Retta r = null;
        Pila pila = new Pila();
        /*algoritmo di Graham*/
        pila.inPila(insieme.get(0));
        pila.inPila(insieme.get(1));
        for (i = 2; i < insieme.size(); i++) {
            r = new Retta(pila.leggiPila(), pila.leggiPila2());
            while (op.stessaParte(r, insieme.get(0), insieme.get(i)) == false) {
                pila.fuoriPila();
                r = new Retta(pila.leggiPila(), pila.leggiPila2());
            }
            pila.inPila(insieme.get(i));
        }
        /*fine algoritmo*/
        /*disegna l'inviluppo convesso*/
        ArrayList<Punto> c = pila.get();
        /*disegna l'insieme dei punti che formano l'inviluppo convesso*/
        for (i = 0; i < c.size(); i++) {
            t = trasforma(c.get(i));
            g.setColor(Color.RED);
            g.fill(t.getArea());
            g.setColor(Color.BLACK);
            g.draw(t.getBordo());
        }
        try {
            Thread.sleep(TIMESTAMP);
        } catch (InterruptedException ex) {
            System.err.println(ex.getMessage());
        }
        /*disegna l'inviluppo convesso*/
        for (i = 1; i < c.size(); i++) {
            disegnaInviluppo(trasforma(c.get(i - 1)), trasforma(c.get(i)), g);
            try {
                Thread.sleep(TIMESTAMP);
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
        disegnaInviluppo(trasforma(c.get(0)), trasforma(c.get(c.size() - 1)), g);
        stato = false;
        insieme.clear();
    }

    /*disegna il segmento dell'inviluppo e i vertici*/
    private void disegnaInviluppo(Punto t1, Punto t2, Graphics2D g) {
        g.setColor(Color.BLUE);
        g.drawLine(t1.x, t1.y, t2.x, t2.y);
        g.setColor(Color.GREEN);
        g.fill(t1.getArea());
        g.fill(t2.getArea());
        g.setColor(Color.BLACK);
        g.draw(t1.getBordo());
        g.draw(t2.getBordo());
    }

    public Punto trasforma(Punto P) {
        return new Punto(P.x, canvas.getHeight() - P.y);
    }
}

