
public class Retta {

    private Punto p1 = null, p2 = null;

    public Retta() {
    }

    public Retta(Punto p1, Punto p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    /*lunghezza del segmento r(p1, p2)*/
    public double modulo() {
        return Math.sqrt(Math.pow((this.p2.getX() - this.p1.getX()), 2) + Math.pow((this.p2.getY() - this.p1.getY()), 2));
    }

    /*angolo tra le retta passanto per "P1"(il primo vertice dell'inviluppo)
    e parallela all'asse x e una retta passantte per un generico punto pi*/
    public double getAlgle() {
        return Math.acos((this.p2.getX() - this.p1.getX()) / modulo());
    }

    public void setP1(Punto p1) {
        this.p1 = p1;

    }

    public void setP2(Punto p2) {
        this.p2 = p2;

    }

    public Punto getP1() {
        return p1;
    }

    public Punto getP2() {
        return p2;
    }

    /*ordino in ordine creascente le rette R(p1,pi) in base all'angolo di rotazione*/
    public static void sort(Retta R[]) {
        Retta tmp;
        for (int k = 1; k < R.length; k++) {
            for (int j = R.length - 1; j >= k; j--) {
                if (R[j - 1].getAlgle() > R[j].getAlgle()) {
                    tmp = R[j - 1];
                    R[j - 1] = R[j];
                    R[j] = tmp;
                }
            }
        }
    }
}
