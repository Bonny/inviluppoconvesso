/*
simulazione di una "pila" di punti mediante un ArrayList
struttura di supporto per l'algoritmo di Graham
 */

import java.util.ArrayList;

public class Pila {

    private ArrayList<Punto> p;

    public Pila() {
        p = new ArrayList<Punto>();
    }

    public ArrayList<Punto> get() {
        return p;
    }

    public void inPila(Punto punto) {
        p.add(punto);
    }

    public Punto leggiPila() {
        return p.get(p.size() - 1);
    }

    public Punto leggiPila2() {
        return p.get(p.size() - 2);
    }

    public void fuoriPila() {
        p.remove(p.size() - 1);
    }

    public boolean isEmpty() {
        return (p.isEmpty());
    }
}
